# Color Wars

Color Wars is a turn based stretegy game.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them:

-   [Node.js](https://nodejs.org)

### Installing

Install the dependencies and grunt.

```
npm install
npm install -g grunt-cli
```

Run grunt task to auto-build the application.

```
grunt watch
```

## Running the tests

Execute the Unit-Tests by running intern.

```
npx intern
```
