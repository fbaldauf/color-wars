import Board from '../../js/model/board';
import Tile from '../../js/model/tile';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

/**
 * @returns Board
 */
const getComponent = () => {
    const component = new Board();
    const iterate = Array(9).fill(0);
    iterate.map((v, x) =>
        iterate.map((v, y) => {
            if (y == 0) component.tiles[x] = [];
            component.tiles[x][y] = new Tile(x, y);
        })
    );
    return component;
};

registerSuite('Board', {
    beforeEach: function() {},
    'create instance'() {
        assert.doesNotThrow(() => getComponent());
    },
    'check if not in grid if negative indexes'() {
        const board = getComponent();
        expect(board.IsInGrid(-1, -1)).to.equal(false);
        expect(board.IsInGrid(0, -1)).to.equal(false);
        expect(board.IsInGrid(-1, 0)).to.equal(false);
    },
    'check if not in grid if too big indexes'() {
        const board = getComponent();
        expect(board.IsInGrid(9, 9)).to.equal(false);
        expect(board.IsInGrid(0, 9)).to.equal(false);
        expect(board.IsInGrid(9, 0)).to.equal(false);
    },
    'check in grid'() {
        const board = getComponent();
        expect(board.IsInGrid(0, 0)).to.equal(true);
        expect(board.IsInGrid(4, 6)).to.equal(true);
        expect(board.IsInGrid(6, 4)).to.equal(true);
        expect(board.IsInGrid(8, 8)).to.equal(true);
    },
    'no adjacents if only one tile in board'() {
        const board = getComponent();
        board.tiles = [[board.tiles.shift()[0]]];

        const actual = board.getAdjacentTiles(board.tiles[0][0]);

        expect(actual).be.an('array');
        expect(actual.length).to.equal(0);
    },
    '6 adjacent tiles'() {
        const board = getComponent();
        const testTile = board.tiles.flat().find(x => x.Col == 4 && x.Row == 2);
        testTile.OwnedBy = {};

        const actual = board.getAdjacentTiles(testTile);

        expect(actual.length).to.equal(6);
    },
    'south west is adjacent tile'() {
        const board = getComponent();
        const testTile = board.tiles.flat().find(x => x.Col == 4 && x.Row == 2);
        testTile.OwnedBy = {};
        const expectedTile = board.tiles
            .flat()
            .find(x => x.Col == 3 && x.Row == 2);

        const actual = board.getAdjacentTiles(testTile);

        expect(actual).be.an('array');
        expect(actual).contains(expectedTile);
    }
});
