import Player from '../../js/model/player';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Player', {
    'create instance'() {
        assert.doesNotThrow(() => new Player());
    },
    'to have the property Color'() {
        const player = new Player();
        expect(player).to.have.property('Color');
    },
    'to have the property Name'() {
        const player = new Player();
        expect(player).to.have.property('Name');
    },
    'Player is active on default'() {
        const player = new Player();
        expect(player.Status).to.equal(1);
    },
    'deactive changes the Status'() {
        const player = new Player();
        player.Status = 1;
        player.deactivate();
        expect(player.Status).to.equal(0);
    },
    'deactive sets Color to noColor'() {
        const player = new Player();
        player.deactivate();
        expect(player.Color).to.be.an('object');
        expect(player.Color.name).to.equal('');
    }
});
