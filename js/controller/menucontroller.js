import ColorWarsGameController from './colorwarsgamecontroller';

function MenuController(model, view) {
    this.model = model;
    this.view = view;

    this.StartApp = function() {
        this.view.Render();
    };

    this.bindView();
}

MenuController.prototype.bindView = function(view) {
    var myView = view || this.view;
    var that = this;

    myView.onMenuEntryClicked.attach(function(sender, data) {
        that.changeSite(data);
    });
};

MenuController.prototype.changeSite = function(site) {
    if (site === 'Start') {
        this.view.onMenuEntryClicked.removeAllListeners();
        var game = new ColorWarsGameController(null, this.view.canvasId);
        game.Start();
    }
};

export default MenuController;
