function SelectionView(model, canvasId, originX, originY) {
    var canvas = document.getElementById(canvasId);
    var context = canvas.getContext('2d');
    this.model = model;
    var that = this;

    this.model.onNewSelection.attach(function() {
        that.Render();
    });

    this.Render = function() {
        for (var i in this.model.getItems()) {
            this.drawItem(this.model.getItems()[i], i * 140);
        }
    };

    this.drawItem = function(item, offset) {
        context.fillStyle = item.color;
        context.fillRect(originX, originY + offset, 120, 120);
    };

    this.getSelection = function(posX, posY) {
        if (posX < 0) {
            return false;
        }
        if (posX > 120) {
            return false;
        }
        if (posY < 0) {
            return false;
        }
        if (posY > this.model.getItems().length * 140) {
            return false;
        }
        if (posY % 140 > 120) {
            return false;
        }

        return this.model.getItems()[Math.floor(posY / 140)];
    };
}

export default SelectionView;
