module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('intern');
    /*
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');*/

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        eslint: {
            target: ['js/**/*.js'],
            options: {
                configFile: 'conf/eslint.json'
            }
        },
        uglify: {
            options: {
                banner:
                    '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'public/js/<%= pkg.name %>.min.js': [
                        'tmp/<%= pkg.name %>.js'
                    ]
                }
            }
        },
        browserify: {
            dist: {
                files: {
                    // destination for transpiled js : source js
                    'tmp/<%= pkg.name %>.js': 'js/**/*.js' //<%= jshint.files %>
                },
                options: {
                    transform: [
                        [
                            'babelify',
                            {
                                presets: [
                                    [
                                        '@babel/preset-env',
                                        {
                                            useBuiltIns: 'entry'
                                        }
                                    ]
                                ]
                            }
                        ]
                    ],
                    browserifyOptions: {
                        debug: true
                    }
                }
            }
        },
        watch: {
            files: ['js/**/*.js', 'tests/**/*.js' /*'<%= jshint.files %>'*/],
            tasks: ['eslint', 'browserify:dist', 'uglify']
        },
        intern: {
            node: {
                options: {
                    suites: 'tests/**/*.js',
                    plugins: 'node_modules/@babel/register/lib/node.js',
                    coverage: 'js/**/*.js',
                    instrumenterOptions: {
                        esModules: true
                    }
                }
            }
        }
    });

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('test', ['intern']);
};
