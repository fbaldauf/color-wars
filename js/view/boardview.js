import TileView from './tileview';

function BoardView(model, canvasId, originX, originY) {
    var canvas = document.getElementById(canvasId);
    var context = canvas.getContext('2d');
    this.model = model;

    this.Render = function() {
        var currentHexX;
        var currentHexY;
        var debugText = '';

        var offsetColumn = false;

        for (var col in this.model.tiles) {
            for (var row in this.model.tiles[col]) {
                var currTileData = this.model.tiles[col][row];
                var currentTile = new TileView(50, currTileData);

                if (!offsetColumn) {
                    currentHexX = currTileData.Col * currentTile.side + originX;
                    currentHexY =
                        currTileData.Row * currentTile.height + originY;
                } else {
                    currentHexX = currTileData.Col * currentTile.side + originX;
                    currentHexY =
                        currTileData.Row * currentTile.height +
                        originY +
                        currentTile.height * 0.5;
                }

                this.drawTile(currentTile, currentHexX, currentHexY, debugText);
            }
            offsetColumn = !offsetColumn;
        }
    };

    this.drawTile = function(tile, x0, y0) {
        tile.Draw(context, x0, y0);
    };
}

export default BoardView;
