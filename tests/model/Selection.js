import Selection from '../../js/model/selection';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Selection', {
    'create instance'() {
        assert.doesNotThrow(() => new Selection());
    },
    'get items returns array'() {
        const selection = new Selection();
        const items = selection.getItems();
        expect(items).to.be.an('array');
    },
    'setNewSelection changed property'() {
        const selection = new Selection();
        const expected = 'whatever';
        selection.setNewSelection(expected);
        const items = selection.getItems();
        expect(items).to.equal(expected);
    },
    'setNewSelection triggers event'() {
        const mock = {
            flag: false,
            notify() {
                this.flag = true;
            }
        };
        const selection = new Selection();
        selection.onNewSelection = mock;

        selection.setNewSelection([]);
        expect(mock.flag).to.equal(true);
    },
    'setNewSelection triggers event with own instance as argument'() {
        const selection = new Selection();
        const mock = {
            flag: null,
            notify(data) {
                this.flag = data;
            }
        };
        selection.onNewSelection = mock;

        selection.setNewSelection([]);
        expect(mock.flag).to.equal(selection);
    }
});
