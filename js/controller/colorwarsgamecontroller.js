import ColorWarsGame from '../model/colorwarsgame';
import ColorWarsGameView from '../view/colorwarsgameview';

function ColorWarsGameController(model, selector) {
    this.model = model;
    this.view = null;
    var canvasId = selector;

    this.Start = function() {
        this.model = this.model || new ColorWarsGame();
        this.model.Setup();
        this.view = new ColorWarsGameView(this.model, canvasId);
        this.view.Render();
        this.bindView();
    };

    this.SelectColor = function(color) {
        this.model.SelectColor(color);
        this.view.Render();
    };
}

ColorWarsGameController.prototype.bindView = function(view) {
    var myView = view || this.view;
    var that = this;

    myView.onColorSelected.attach(function(sender, data) {
        that.SelectColor(data);
    });
};

export default ColorWarsGameController;
