class Board {
    constructor() {
        this.tiles = [];
    }

    getAdjacentTiles(refTile) {
        refTile.flag = true;
        var adjacents = [];
        var x = refTile.Col;
        var y = refTile.Row;

        // Links unten
        adjacents = this.AddTileIfInGrid(
            adjacents,
            x - 1,
            refTile.isEvenX() ? y : y + 1,
            refTile
        );

        // Links oben
        adjacents = this.AddTileIfInGrid(
            adjacents,
            x - 1,
            refTile.isEvenX() ? y - 1 : y,
            refTile
        );

        // Rechts unten
        adjacents = this.AddTileIfInGrid(
            adjacents,
            x + 1,
            refTile.isEvenX() ? y : y + 1,
            refTile
        );

        // Links oben
        adjacents = this.AddTileIfInGrid(
            adjacents,
            x + 1,
            refTile.isEvenX() ? y - 1 : y,
            refTile
        );

        // Unten
        adjacents = this.AddTileIfInGrid(adjacents, x, y + 1, refTile);

        // Oben
        adjacents = this.AddTileIfInGrid(adjacents, x, y - 1, refTile);

        adjacents.forEach(function(item) {
            if (!item.flag) {
                // Rekursiv aufrufen
                if (refTile.OwnedBy === item.OwnedBy) {
                    this.getAdjacentTiles(item).forEach(function(item) {
                        adjacents.push(item);
                    });
                }
            }
        }, this);

        return adjacents;
    }

    IsInGrid(x, y) {
        if (x < 0 || y < 0) return false;
        if (x >= this.tiles.length) return false;
        if (y >= this.tiles[0].length) return false;

        return true;
    }

    _getMostXValue() {
        return Math.max(...this.tiles.map(x => x.Col));
    }

    _getMostYValue() {
        return Math.max(...this.tiles.map(x => x.Row));
    }

    AddTileIfInGrid(adjacents, x, y) {
        if (this.IsInGrid(x, y)) {
            // if (this.tiles[x][y].Color !== Colors.noColor) {
            if (
                !this.tiles[x][y].OwnedBy ||
                this.tiles[x][y].OwnedBy.Status !== 1
            ) {
                adjacents.push(this.tiles[x][y]);
                // }
            }
        }

        return adjacents;
    }
}

export default Board;
