import MVCEvent from '../common/event';

class Selection {
    constructor() {
        this.items = [];
        this.onNewSelection = new MVCEvent(this);
    }

    getItems() {
        return this.items;
    }

    setNewSelection(data) {
        this.onNewSelection.notify(this);
        this.items = data;
    }
}

export default Selection;
