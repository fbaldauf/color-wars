function TileView(radius, pData) {
    this.radius = radius;

    this.height = Math.sqrt(3) * radius;
    this.width = 2 * radius;
    this.side = (3 / 2) * radius;
    this.color = null;

    var data = pData;

    this.Draw = function(context, x0, y0) {
        context.strokeStyle = '#000';

        if (data.Status === 1) {
            context.lineWidth = 6;
        } else {
            context.lineWidth = 2;
        }

        context.beginPath();
        context.moveTo(x0 + this.width - this.side, y0);
        context.lineTo(x0 + this.side, y0);
        context.lineTo(x0 + this.width, y0 + this.height / 2);
        context.lineTo(x0 + this.side, y0 + this.height);
        context.lineTo(x0 + this.width - this.side, y0 + this.height);
        context.lineTo(x0, y0 + this.height / 2);

        if (data.Color) {
            context.fillStyle = data.Color.color;
            if (data.OwnedBy) {
                if (data.OwnedBy.Status === 0) {
                    context.fillStyle = '#FFFFFF';
                } else {
                    context.fillStyle = data.Color.active;
                }
            }
            context.fill();
        }

        context.closePath();
        context.stroke();

        context.font = '8px';
        context.fillStyle = '#000';

        context.fillText(
            data.Col + ',' + data.Row,
            x0 + this.width / 2 - this.width / 4,
            y0 + (this.height - 5)
        );
        // context.fillText((data.OwnedBy && data.OwnedBy.Name) || '', x0 + (this.width / 2) - (this.width / 4), y0 + (this.height - 5))
    };
}

export default TileView;
