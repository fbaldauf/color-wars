var hexagonColors = [
    {
        name: 'orange',
        color: '#F44336',
        active: '#D32F2F'
    },
    {
        name: 'pruple',
        color: '#673AB7',
        active: '#512DA8'
    },
    {
        name: 'sky',
        color: '#2196F3',
        actice: '#1976D2'
    },
    {
        name: 'darkgreen',
        color: '#009688',
        active: '#00796B'
    },
    {
        name: 'yellow',
        color: '#CDDC39',
        active: '#AFB42B'
    },
    {
        name: 'brown',
        color: '#795548',
        active: '#5D4037'
    },
    {
        name: 'gray',
        color: '#607D8B',
        active: '#455A64'
    }
];

var noColor = { name: '', color: '', active: '' };

export default {
    getRandom: function() {
        return hexagonColors[
            Math.floor(Math.random() * hexagonColors.length + 1) - 1
        ];
    },
    noColor: noColor
};
