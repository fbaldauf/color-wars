import Colors from '../common/colors';

class Tile {
    constructor(col, row) {
        this.Col = col;
        this.Row = row;
        this.Color = null;
        this.Status = 0;
        this.OwnedBy = null;
    }

    isEvenX() {
        return this.Col % 2 === 0;
    }

    randomizeColor() {
        this.Color = Colors.getRandom();
    }
}

export default Tile;
