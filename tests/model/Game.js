import Game from '../../js/model/colorwarsgame';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Game', {
    'create instance'() {
        assert.doesNotThrow(() => new Game());
    },
    '2 Player mode on default'() {
        const game = new Game();

        game.Setup();
        const actual = game.Players.length;

        expect(actual).to.equal(2);
    },
    'First Player starts'() {
        const game = new Game();

        game.Setup();
        const expected = game.Players[0];
        const actual = game.CurrentPlayer;

        expect(actual).to.equal(expected);
    },
    'Board is loaded after setup'() {
        const game = new Game();

        game.Setup();
        const actual = game.Board;

        expect(actual).to.be.a('object');
    },
    'Selection is loaded after setup'() {
        const game = new Game();

        game.Setup();
        const actual = game.Selection;

        expect(actual).to.be.a('object');
    }
});
