import Menu from './model/menu';
import MenuView from './view/menuview';
import MenuController from './controller/menucontroller';

document.addEventListener('DOMContentLoaded', () => {
    var model = new Menu();
    var view = new MenuView(model, 'HexCanvas');
    var controller = new MenuController(model, view);

    controller.StartApp();
    // DEBUG: Start direct
    // controller.changeSite('Start')
});
