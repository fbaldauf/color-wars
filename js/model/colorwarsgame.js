import MVCEvent from '../common/event';
import Board from '../model/board';
import Player from './player';
import Selection from './selection';
import Tile from './tile';

class Game {
    constructor() {
        this.Players = [];
        this.CurrentPlayer = null;
        this.Board = null;
        this.Selection = null;

        this.onNextTurn = new MVCEvent(this);
    }
    Setup() {
        // Optionen
        var options = {
            cols: 19,
            rows: 9
        };

        // Spieler anlegen
        var player1 = new Player();
        player1.Name = 'Ulf';
        player1.StartX = 0;
        player1.StartY = 0;

        var player2 = new Player();
        player2.Name = 'Anton';
        player2.StartX = options.cols - 1;
        player2.StartY = 0;

        this.Players = [player1, player2];

        // Spielfeld vorbereiten
        this.CurrentPlayer = this.Players[0];
        this.Board = this.CreateNewBoard(19, 9);
        this.CurrentPlayer.Color = this.Board.tiles[this.Players[0].StartX][
            this.Players[0].StartY
        ].Color;

        this.Players.forEach(function(player) {
            this.Board.tiles[player.StartX][player.StartY].OwnedBy = player;
            player.Color = this.Board.tiles[player.StartX][player.StartY].Color;
        }, this);

        // Startaufstellung - Fixing Randoms
        while (this.Board.tiles[1][0].Color === this.Players[0].Color) {
            this.Board.tiles[1][0].randomizeColor();
        }
        while (this.Board.tiles[0][1].Color === this.Players[0].Color) {
            this.Board.tiles[0][1].randomizeColor();
        }

        while (
            this.Board.tiles[this.Players[1].StartX - 1][0].Color ===
            this.Players[1].Color
        ) {
            this.Board.tiles[this.Players[1].StartX - 1][0].randomizeColor();
        }
        while (
            this.Board.tiles[this.Players[1].StartX][1].Color ===
            this.Players[1].Color
        ) {
            this.Board.tiles[this.Players[1].StartX][1].randomizeColor();
        }

        // Start Runde 1
        this.SelectTilesOfCurrentPlayer();

        this.Selection = new Selection();
        this.RefreshSelection();
    }

    SelectColor(color) {
        var i = 0;
        var ownTiles = null;

        while ((ownTiles = this.GetAllTilesOfCurrentPlayer()).length > i) {
            i = ownTiles.length;
            ownTiles.forEach(function(item) {
                var adjacents = this.Board.getAdjacentTiles(item);
                adjacents
                    .filter(function(item) {
                        if (item.Color === color && item.OwnedBy === null) {
                            return item;
                        }
                    })
                    .forEach(function(item) {
                        item.OwnedBy = this.CurrentPlayer;
                    }, this);
            }, this);
        }

        this.GetAllTilesOfCurrentPlayer().forEach(function(item) {
            if (item.OwnedBy === this.CurrentPlayer) {
                item.Color = color;
            }
        }, this);

        this.CurrentPlayer.Color = color;
        this.NextTurn();
    }

    NextTurn() {
        if (this.CheckEnd()) {
            return false;
        }
        this.ChangePlayer();
        this.UnselectAll();
        this.SelectTilesOfCurrentPlayer();
        this.RefreshSelection();

        // Notify View
        this.onNextTurn.notify();
    }

    CheckEnd() {
        if (this.getEmptyTiles().length === 0) {
            alert(this.getWinner().Name + ' won!');
            return true;
        }
    }

    getWinner() {
        var maxPoints = 0;
        var winner = null;
        this.Players.forEach(function(item) {
            //console.log(item);
            //console.log(this.GetAllTilesOfPlayer(item));
            var points = this.GetAllTilesOfPlayer(item).length;
            if (points > maxPoints) {
                maxPoints = points;
                winner = item;
            }
        }, this);

        return winner;
    }

    getEmptyTiles() {
        var tiles = [];
        this.Board.tiles.forEach(function(items) {
            items.forEach(function(item) {
                if (!item.OwnedBy) {
                    tiles.push(item);
                }
            });
        });

        return tiles;
    }

    ChangePlayer() {
        var nextIndex = this.Players.indexOf(this.CurrentPlayer) + 1;
        if (nextIndex >= this.Players.length) {
            nextIndex = 0;
        }
        this.CurrentPlayer = this.Players[nextIndex];
        this.CurrentPlayer.Color = this.GetStartTileOfCurrentPlayer().Color;
    }

    UnselectAll() {
        this.Board.tiles.forEach(function(items) {
            items.forEach(function(item) {
                item.Status = 0;
            });
        }, this);
    }

    SelectTilesOfCurrentPlayer() {
        this.GetAllTilesOfCurrentPlayer().forEach(function(item) {
            if (item.OwnedBy === this.CurrentPlayer) {
                item.Status = 1;
            }
        }, this);
    }

    CreateNewBoard(cols, rows) {
        var board = new Board();
        for (var i = 0; i < cols; i++) {
            board.tiles[i] = [];
            for (var j = 0; j < rows; j++) {
                board.tiles[i][j] = new Tile(i, j);
                board.tiles[i][j].randomizeColor();
            }
        }

        return board;
    }

    GetStartTileOfCurrentPlayer() {
        return this.Board.tiles[this.CurrentPlayer.StartX][
            this.CurrentPlayer.StartY
        ];
    }

    GetAllTilesOfPlayer(player) {
        var tiles = [];
        this.Board.tiles.forEach(function(items) {
            items
                .filter(function(item) {
                    // console.log(item.OwnedBy)
                    // console.log(player)
                    if (item.OwnedBy === player) {
                        return item;
                    }
                }, this)
                .forEach(function(item) {
                    tiles.push(item);
                });
        }, this);

        return tiles;
    }

    GetAllTilesOfCurrentPlayer() {
        return this.GetAllTilesOfPlayer(this.CurrentPlayer);
    }

    RefreshSelection() {
        //console.log('Current Player: ' + this.CurrentPlayer.Name);
        //console.log('Current Color: ' + this.CurrentPlayer.Color.color);

        var items = [];
        var ownTiles = this.GetAllTilesOfCurrentPlayer();
        ownTiles.forEach(function(item) {
            var adjacents = this.Board.getAdjacentTiles(item);
            adjacents
                .filter(function(item) {
                    if (item.Color !== this.CurrentPlayer.Color) {
                        return item;
                    }
                }, this)
                .forEach(function(item) {
                    if (items.indexOf(item.Color) < 0) {
                        items.push(item.Color);
                    }
                }, this);
        }, this);

        // Gegnerfarben dürfen nicht gewählt werden
        items = items.filter(function(item) {
            for (var i in this.Players) {
                if (this.Players[i].Color === item) {
                    return null;
                }
            }

            return item;
        }, this);

        if (items.length === 0) {
            this.CurrentPlayer.deactivate();
            this.NextTurn();
        } else {
            this.Selection.setNewSelection(items);
        }
    }
}

export default Game;
