class MVCEvent {
    constructor(sender) {
        this.sender = sender;
        this.listeners = [];
    }

    // add listener closures to the list
    attach(listener) {
        this.listeners.push(listener);
    }

    // loop through, calling attached listeners
    notify(args) {
        this.listeners.forEach(listener => {
            listener(this.sender, args);
        });
    }

    // removes all listeners
    removeAllListeners() {
        this.listeners = [];
    }
}

export default MVCEvent;
