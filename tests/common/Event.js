import Event from '../../js/common/event';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Event', {
    'create instance'() {
        assert.doesNotThrow(() => new Event());
    },
    'check if notify will trigger all listeners once'() {
        let flag1 = 0;
        const listener1 = () => {
            flag1++;
        };
        let flag2 = 0;
        const listener2 = () => {
            flag2++;
        };
        const event = new Event();
        event.attach(listener1);
        event.attach(listener2);
        event.notify();
        expect(flag1).to.equal(1);
        expect(flag2).to.equal(1);
    },
    'check removeAllListeners'() {
        let flag = 0;
        const listener = () => {
            flag++;
        };
        const event = new Event();
        event.attach(listener);
        event.removeAllListeners();
        event.notify();
        expect(flag).to.equal(0);
    },
    'check sender object'() {
        const expected = { id: 1 };
        let flag = null;
        const listener = data => {
            flag = data;
        };
        const event = new Event(expected);
        event.attach(listener);
        event.notify();
        expect(flag).to.equal(expected);
    },
    'check event args'() {
        const expected = 'my value';
        let flag = null;
        const listener = (sender, data) => {
            flag = data;
        };
        const event = new Event();
        event.attach(listener);
        event.notify(expected);
        expect(flag).to.equal(expected);
    }
});
