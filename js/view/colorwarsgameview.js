import MVCEvent from '../common/event';
import BoardView from './boardview';
import SelectionView from './selectionview';

function ColorWarsGameView(model, canvasId) {
    this.model = model;
    this.canvasId = canvasId;
    var canvas = document.getElementById(this.canvasId);
    var ctx = canvas.getContext('2d');

    var board = new BoardView(this.model.Board, this.canvasId, 25, 60);
    var selection = new SelectionView(
        this.model.Selection,
        this.canvasId,
        1600,
        100
    );

    //   var width = 600
    //   var padding = 15
    //   var itemheight = 50

    // attach model listeners
    // this.model.onSet.attach(
    //  () => this.show()
    // )

    // for 2-way binding
    this.onColorSelected = new MVCEvent(this);
    var that = this;

    canvas.addEventListener(
        'mousedown',
        function(e) {
            that.checkSelect(e);
        },
        false
    );
    this.checkSelect = function(e) {
        var item = selection.getSelection(e.pageX - 1600, e.pageY - 100);
        if (item) {
            this.onColorSelected.notify(item);
        }
    };

    this.Render = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.font = '14px';
        ctx.fillStyle = this.model.CurrentPlayer.Color.color;
        ctx.textAlign = 'left';
        ctx.textBaseline = 'alphabetic';
        ctx.fillText(this.model.CurrentPlayer.Name + '`s turn', 1600, 50);
        board.Render();
        selection.Render();
    };
}

export default ColorWarsGameView;
