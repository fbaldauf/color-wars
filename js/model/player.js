import Colors from '../common/colors';

class Player {
    constructor() {
        this.Status = 1;
        this.Color = null;
        this.Name = null;

        //TODO: refactor the following 2 properties out of this class
        this.StartX = null;
        this.StartY = null;
    }

    deactivate() {
        this.Status = 0;
        this.Color = Colors.noColor;
    }
}

export default Player;
