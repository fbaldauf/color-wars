import Tile from '../../js/model/tile';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Tiles', {
    'create new'() {
        assert.doesNotThrow(() => new Tile());
    },
    'create new with col argument'() {
        const expected = 5;
        const tile = new Tile(expected);
        expect(tile.Col).to.equal(expected);
    },
    'create new with row argument'() {
        const expected = 5;
        const tile = new Tile(null, expected);
        expect(tile.Row).to.equal(expected);
    },
    'isEvenX to return true when even object'() {
        const tile = new Tile();
        tile.Col = 0;
        expect(tile.isEvenX()).to.equal(true);
    },
    'isEvenX to return false when odd object'() {
        const tile = new Tile();
        tile.Col = 1;
        expect(tile.isEvenX()).to.equal(false);
    },
    'Color property to be null after init'() {
        const tile = new Tile();
        expect(tile.Color).to.equal(null);
    },
    'randomizeColor to change Color property'() {
        const tile = new Tile();
        tile.randomizeColor();
        expect(tile.Color).to.be.an('object');
    },
    'to have the property Status'() {
        const tile = new Tile();
        expect(tile).to.have.property('Status');
    },
    'to have the property OwnedBy'() {
        const tile = new Tile();
        expect(tile).to.have.property('OwnedBy');
    }
});
