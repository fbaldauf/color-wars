import Menu from '../../js/model/menu';

const { assert } = intern.getPlugin('chai');
const { expect } = intern.getPlugin('chai');
const { registerSuite } = intern.getPlugin('interface.object');

registerSuite('Menu', {
    'create instance'() {
        assert.doesNotThrow(() => new Menu());
    },
    'to have entries'() {
        const menu = new Menu();
        expect(menu.Entries).to.be.an('array');
    }
});
