import MVCEvent from '../common/event';

function MenuView(model, canvasId) {
    this.model = model;
    this.canvasId = canvasId;
    var canvas = document.getElementById(this.canvasId);
    var ctx = canvas.getContext('2d');
    var width = 600;
    var padding = 15;
    var itemheight = 50;

    // attach model listeners
    // this.model.onSet.attach(
    //  () => this.show()
    // )

    // for 2-way binding
    this.onMenuEntryClicked = new MVCEvent(this);

    this.getHeight = function() {
        return this.model.Entries.length * (itemheight + padding);
    };

    this.Render = function() {
        ctx.fillStyle = 'teal';
        ctx.fillRect(0, 0, width, this.getHeight() + padding);
        for (var i = 0; i < this.model.Entries.length; i++) {
            this.RenderItem(this.model.Entries[i], i);
        }
    };

    this.RenderItem = function(Item, y) {
        ctx.fillStyle = 'black';
        ctx.fillRect(
            padding,
            padding + y * (itemheight + padding),
            width - 2 * padding,
            itemheight
        );
        var middle = padding + y * (itemheight + padding) + itemheight / 2;
        ctx.font = '30px Segoe UI';
        ctx.fillStyle = 'red';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(Item, width / 2, middle);
    };

    this.isInBounds = function(x, y) {
        return x >= 0 && x <= width && y >= 0 && y <= this.getHeight();
    };

    this.hitsButton = function(x, y) {
        if (!this.isInBounds(x, y)) {
            return false;
        }

        if (x <= padding || x >= width - padding) {
            return false;
        }

        if (y % (itemheight + padding) < padding) {
            return false;
        }

        var buttonNr = Math.floor(y / (itemheight + padding));
        return buttonNr;
    };

    this.clickEvent = function(e) {
        var mouseX = e.offsetX;
        var mouseY = e.offsetY;
        var btnClicked = this.hitsButton(mouseX, mouseY);

        if (btnClicked !== false) {
            this.onMenuEntryClicked.notify(this.model.Entries[btnClicked]);
        }
    };

    canvas.addEventListener('mousedown', this.clickEvent.bind(this), false);
}

export default MenuView;
